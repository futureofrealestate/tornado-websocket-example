FROM ubuntu

WORKDIR /tmp/tornado-websocket-example

RUN apt-get update -y && apt-get install git python python-pip -y

COPY ./requirements.txt .

RUN pip install -r requirements.txt

COPY . .

EXPOSE 8888

CMD ["python", "/tmp/tornado-websocket-example/app.py"]
